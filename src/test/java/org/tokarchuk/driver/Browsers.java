package org.tokarchuk.driver;

public enum Browsers {
    CHROME,
    FIREFOX,
    EDGE
}
