package org.tokarchuk.tests.windows;

import org.tokarchuk.tests.base.BaseTest;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;


import java.util.Set;

public class WindowsTest extends BaseTest {

    @BeforeMethod
    public void beforeMethod() {
        driver.get("http://the-internet.herokuapp.com/windows");
    }

    @Test
    public void windowsTest() {

        String mainWindowHandle = driver.getWindowHandle();
        driver.findElement(By.xpath("//a[contains(text(),'Click Here')]")).click();

        Set<String> windowHandles = driver.getWindowHandles();
        for (String handle : windowHandles) {
            if (!handle.equals(mainWindowHandle)) {
                driver.switchTo().window(handle);
                break;
            }
        }

        String text = driver.findElement(By.xpath("//h3[contains(text(),'New Window')]")).getText();
        String currentUrl = driver.getCurrentUrl();

        Assert.assertEquals(text, "New Window");
        Assert.assertTrue(currentUrl.endsWith("/new"));

        driver.close();

        driver.switchTo().window(mainWindowHandle);

        text = driver.findElement(By.xpath("//h3[contains(text(),'Opening a new window')]")).getText();

        Assert.assertEquals(text, "Opening a new window");
        Assert.assertTrue(driver.findElement(By.xpath("//a[contains(text(),'Click Here')]")).isDisplayed());
    }

}
