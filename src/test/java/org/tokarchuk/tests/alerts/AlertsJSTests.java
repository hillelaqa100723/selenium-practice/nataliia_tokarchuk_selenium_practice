package org.tokarchuk.tests.alerts;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class AlertsJSTests {

    WebDriver driver;

    @BeforeClass
    public void beforeClass(){
        driver = new FirefoxDriver();
    }

    @AfterClass(alwaysRun = true)
    public void afterClass() {
        if (driver != null) {
            driver.quit();
        }
    }

    @BeforeMethod
    public void beforeMethod() {
        driver.get("http://the-internet.herokuapp.com/javascript_alerts");
    }


    @Test(priority = 1)
    public void clickJSConfirmOkTest() {
        String expectedTextConfirm1 = "I am a JS Confirm";
        String expectedResultText1 = "You clicked: Ok";

        clickJSButton(Buttons.JS_CONFIRM);
        String text = workWithAlert(true);

        Assert.assertEquals(text, expectedTextConfirm1);
        Assert.assertEquals(getResultText(), expectedResultText1);

    }

    @Test(priority = 2)
    public void clickJSConfirmCancelTest() {
        String expectedTextConfirm2 = "I am a JS Confirm";
        String expectedResultText2 = "You clicked: Cancel";

        clickJSButton(Buttons.JS_CONFIRM);
        String text = workWithAlert(false);

        Assert.assertEquals(text, expectedTextConfirm2);
        Assert.assertEquals(getResultText(), expectedResultText2);

    }

    @Test(priority = 3)
    public void clickJSPromptTest() {
        String expectedTextAtAlert = "I am a JS prompt";
        String testToEnter = "Hillel School!";

        clickJSButton(Buttons.JS_PROMPT);

        String textFromAlert = workWithAlert(true, testToEnter);

        String resultText = getResultText();

        Assert.assertEquals(textFromAlert, expectedTextAtAlert);
        Assert.assertEquals("You entered: " + testToEnter, resultText);
    }

    @Test(priority = 4)
    public void clickJSPromptTest2() {
        String expectedTextAtAlert = "I am a JS prompt";
        String testToEnter = "";

        clickJSButton(Buttons.JS_PROMPT);

        String textFromAlert = workWithAlert(true, testToEnter);

        String resultText = getResultText();

        Assert.assertEquals(textFromAlert, expectedTextAtAlert);
        Assert.assertEquals("You entered:" + testToEnter, resultText);
    }

    @Test(priority = 5)
    public void clickJSPromptCancelTest() {
        String expectedTextAtAlert = "I am a JS prompt";
        String testToEnter = "Hillel School!";

        clickJSButton(Buttons.JS_PROMPT);
        String textFromAlert = workWithAlert(false, testToEnter);

        String resultText = getResultText();

        Assert.assertEquals(textFromAlert, expectedTextAtAlert);
        Assert.assertEquals("You entered: " + null, resultText);
    }

    @Test(priority = 6)
    public void clickJSPromptCancelTest2() {
        String expectedTextAtAlert = "I am a JS prompt";
        String testToEnter = "";

        clickJSButton(Buttons.JS_PROMPT);
        String textFromAlert = workWithAlert(false, testToEnter);

        String resultText = getResultText();

        Assert.assertEquals(textFromAlert, expectedTextAtAlert);
        Assert.assertEquals("You entered: " + null, resultText);
    }

    @Test
    public void jsAlertWithJSTest() {
        String expectedTextAtAlert = "I am a JS Alert";
        String expectedResultText = "You successfully clicked an alert";


        clickJSButtonCallingJS(Buttons.JS_ALERT);
        String text = workWithAlert(true);


        Assert.assertEquals(text, expectedTextAtAlert);
        Assert.assertEquals(getResultTextWithJs(), expectedResultText);
    }

    @Test
    public void jsConfirmWithJSTest() {
        String expectedTextAtAlert = "I am a JS Confirm";
        String expectedResultText = "You clicked: Ok";


        clickJSButtonCallingJS(Buttons.JS_CONFIRM);
        String text = workWithAlert(true);


        Assert.assertEquals(text, expectedTextAtAlert);
        Assert.assertEquals(getResultTextWithJs(), expectedResultText);
    }

    @Test
    public void jsConfirmWithJSTest2() {
        String expectedTextAtAlert = "I am a JS Confirm";
        String expectedResultText = "You clicked: Cancel";


        clickJSButtonCallingJS(Buttons.JS_CONFIRM);
        String text = workWithAlert(false);


        Assert.assertEquals(text, expectedTextAtAlert);
        Assert.assertEquals(getResultTextWithJs(), expectedResultText);
    }

    @Test
    public void jsWithJSPromptTest() {
        String expectedTextAtAlert = "I am a JS prompt";
        String testToEnter = "You entered: Hillel School!";


        clickJSButtonCallingJS(Buttons.JS_PROMPT);
        String text = workWithAlert(false, testToEnter);

        String resultText = getResultTextWithJs();

        Assert.assertEquals(text, expectedTextAtAlert);
        Assert.assertEquals("You entered: " + null, resultText);
    }

    @Test
    public void jsWithJSPromptTest2() {
        String expectedTextAtAlert = "I am a JS prompt";
        String testToEnter = "You entered: ";

        clickJSButtonCallingJS(Buttons.JS_PROMPT);
        String text = workWithAlert(false, testToEnter);

        String resultText = getResultTextWithJs();

        Assert.assertEquals(text, expectedTextAtAlert);
        Assert.assertEquals("You entered: " + null, resultText);
    }

    private void clickJSButton(Buttons buttons){
        findButton(buttons).click();
    }

    private void clickJSButtonCallingJS(Buttons buttons) {
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        WebElement button = findButton(buttons);
        executor.executeScript("return arguments[0].click();", button);
    }

    private WebElement findButton(Buttons button){
        return driver.findElement(By.xpath("//button[text()='%s']".formatted(button.getTextValue())));
    }

    private String workWithAlert(boolean accept, String... textToInput){
        Alert alert = driver.switchTo().alert();
        String text = alert.getText();

        if (textToInput.length > 0){
            alert.sendKeys(textToInput[0]);
        }

        if (accept){
            alert.accept();
        } else {
            alert.dismiss();
        }
        return text;
    }

    private String getResultText(){
        return driver.findElement(By.id("result")).getText();
    }

    private String getResultTextWithJs() {
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        WebElement element = driver.findElement(By.id("result"));
        return executor.executeScript("return arguments[0].textContent;", element).toString();
    }


    enum Buttons {
        JS_ALERT("Click for JS Alert", "jsAlert()"),
        JS_CONFIRM("Click for JS Confirm", "jsConfirm()"),
        JS_PROMPT("Click for JS Prompt", "jsPrompt()");

        private String textValue;
        private String code;

        Buttons(String textValue, String code) {
            this.textValue = textValue;
            this.code = code;
        }

        public String getTextValue() {
            return textValue;
        }

        public String getCode() {
            return code;
        }
    }
}
