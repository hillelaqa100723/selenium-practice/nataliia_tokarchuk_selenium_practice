package org.tokarchuk.tests.alerts;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class AlertsTests {

    WebDriver driver;

    @BeforeClass
    public void beforeClass(){
        driver = new FirefoxDriver();
    }

    @AfterClass(alwaysRun = true)
    public void afterClass() {
        if (driver != null) {
            driver.quit();
        }
    }

    @BeforeMethod
    public void beforeMethod() {
        driver.get("http://the-internet.herokuapp.com/javascript_alerts");
    }


    @Test(priority = 1)
    public void clickJSConfirmOkTest() {
        String expectedTextConfirm1 = "I am a JS Confirm";
        String expectedResultText1 = "You clicked: Ok";

        clickJSButton(Buttons.JS_CONFIRM);
        String text = workWithAlert(true);

        Assert.assertEquals(text, expectedTextConfirm1);
        Assert.assertEquals(getResultText(), expectedResultText1);

    }

    @Test(priority = 2)
    public void clickJSConfirmCancelTest() {
        String expectedTextConfirm2 = "I am a JS Confirm";
        String expectedResultText2 = "You clicked: Cancel";

        clickJSButton(Buttons.JS_CONFIRM);
        String text = workWithAlert(false);

        Assert.assertEquals(text, expectedTextConfirm2);
        Assert.assertEquals(getResultText(), expectedResultText2);

    }

    @Test(priority = 3)
    public void clickJSPromptTest() {
        String expectedTextAtAlert = "I am a JS prompt";
        String testToEnter = "Hillel School!";

        clickJSButton(Buttons.JS_PROMPT);

        String textFromAlert = workWithAlert(true, testToEnter);

        String resultText = getResultText();

        Assert.assertEquals(textFromAlert, expectedTextAtAlert);
        Assert.assertEquals("You entered: " + testToEnter, resultText);
    }

    @Test(priority = 4)
    public void clickJSPromptTest2() {
        String expectedTextAtAlert = "I am a JS prompt";
        String testToEnter = "";

        clickJSButton(Buttons.JS_PROMPT);

        String textFromAlert = workWithAlert(true, testToEnter);

        String resultText = getResultText();

        Assert.assertEquals(textFromAlert, expectedTextAtAlert);
        Assert.assertEquals("You entered:" + testToEnter, resultText);
    }

    @Test(priority = 5)
    public void clickJSPromptCancelTest() {
        String expectedTextAtAlert = "I am a JS prompt";
        String testToEnter = "Hillel School!";

        clickJSButton(Buttons.JS_PROMPT);
        String textFromAlert = workWithAlert(false, testToEnter);

        String resultText = getResultText();

        Assert.assertEquals(textFromAlert, expectedTextAtAlert);
        Assert.assertEquals("You entered: " + null, resultText);
    }

    @Test(priority = 6)
    public void clickJSPromptCancelTest2() {
        String expectedTextAtAlert = "I am a JS prompt";
        String testToEnter = "";

        clickJSButton(Buttons.JS_PROMPT);
        String textFromAlert = workWithAlert(false, testToEnter);

        String resultText = getResultText();

        Assert.assertEquals(textFromAlert, expectedTextAtAlert);
        Assert.assertEquals("You entered: " + null, resultText);
    }

    private void clickJSButton(Buttons buttons){
        findButton(buttons).click();
    }
    private WebElement findButton(Buttons button){
        return driver.findElement(By.xpath("//button[text()='%s']".formatted(button.getTextValue())));
    }

    private String workWithAlert(boolean accept, String... textToInput){
        Alert alert = driver.switchTo().alert();
        String text = alert.getText();

        if (textToInput.length > 0){
            alert.sendKeys(textToInput[0]);
        }

        if (accept){
            alert.accept();
        } else {
            alert.dismiss();
        }
        return text;
    }

    private String getResultText(){
        return driver.findElement(By.id("result")).getText();
    }

    enum Buttons {
        JS_ALERT("Click for JS Alert", "jsAlert()"),
        JS_CONFIRM("Click for JS Confirm", "jsConfirm()"),
        JS_PROMPT("Click for JS Prompt", "jsPrompt()");

        private String textValue;
        private String code;

        Buttons(String textValue, String code) {
            this.textValue = textValue;
            this.code = code;
        }

        public String getTextValue() {
            return textValue;
        }
    }
}
