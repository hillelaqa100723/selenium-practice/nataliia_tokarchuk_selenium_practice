package org.tokarchuk.tests;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.tokarchuk.driver.WebDriverFactory;
import org.tokarchuk.utils.MyFileUtils;

public class BaseClass {
    protected WebDriver driver;

    @BeforeSuite
    public void beforeClass() {
        MyFileUtils.getDownloadsFolder();
        driver = WebDriverFactory.initDriver();
    }

    @AfterSuite(alwaysRun = true)
    public void afterClass() {
        if (driver != null) {
            driver.quit();
        }
    }
}

