package org.tokarchuk.tests.frames;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.*;


public class FrameTests {
    WebDriver driver;

    @BeforeClass
    public void beforeClass() {
        driver = new ChromeDriver();
    }

    @AfterClass(alwaysRun = true)
    public void afterClass() {
        if (driver != null) {
            driver.quit();
        }
    }

    @BeforeMethod
    public void beforeMethod() {
        driver.get("http://the-internet.herokuapp.com/nested_frames");
    }

    @Test(dataProvider = "dataProvider")
    public void frameText(String frameName, String expectedText) {
        if (!frameName.equals("frame-bottom")) {
            driver.switchTo().frame("frame-top");
        }

        driver.switchTo().frame(frameName);

        WebElement frameTextElement = driver.findElement(By.tagName("body"));
        String frameText = frameTextElement.getText().trim();
        Assert.assertEquals(frameText, expectedText);

        driver.switchTo().defaultContent();
    }


    @DataProvider(name = "dataProvider")
    public Object[][] dataProvider() {
        return new Object[][]{
                {"frame-left", "LEFT"},
                {"frame-middle", "MIDDLE"},
                {"frame-right", "RIGHT"},
                {"frame-bottom", "BOTTOM"}
        };
    }

}