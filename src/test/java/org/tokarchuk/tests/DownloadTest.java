package org.tokarchuk.tests;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;

public class DownloadTest extends BaseClass {

    @DataProvider(name = "amountData")
    public Object[][] amountData() {
        return new Object[][]{
                {"2", "Paragraphs"},
                {"120", "Words"},
                {"20", "Sentences"},
        };
    }

    @Test(dataProvider = "amountData")
    public void DownloadTest2(String amountInput, String amountType) throws IOException {
        driver.get("https://www.webfx.com/tools/lorem-ipsum-generator/");
        WebElement amountInputField = driver.findElement(By.id("amount_generator"));
        amountInputField.clear();
        amountInputField.sendKeys(amountInput);
        WebElement amountTypeDropdown = driver.findElement(By.name("type"));
        Select select = new Select(amountTypeDropdown);
        select.selectByVisibleText(amountType);
        WebElement generateButton = driver.findElement(By.xpath("//div[@id='form-actions1']/button[@type='submit']"));
        generateButton.click();

        WebElement generatedTextElement = driver.findElement(By.id("result_field"));
        String generatedText = generatedTextElement.getText();

        driver.get("https://demo.seleniumeasy.com/generate-file-to-download-demo.html");
        WebElement enterDataTextArea = driver.findElement(By.id("textbox"));
        enterDataTextArea.sendKeys(generatedText);
        WebElement generateFileButton = driver.findElement(By.id("create"));
        generateFileButton.click();
        WebElement downloadLink = driver.findElement(By.id("link-to-download"));
        downloadLink.click();


        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(120));
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("loader")));



        File downloadFolder = new File("/Users/nataliia/Documents/nataliia_tokarchuk_selenium_practice/downloads");
        File[] downloadedFiles = downloadFolder.listFiles();

        for (File file : downloadedFiles) {
            if (file.isFile()) {
                String fileName = file.getName();
                String fileContent = readTextFromFile(file.getAbsolutePath());

                if (fileContent.equals(generatedText)) {
                    System.out.println("Fail " + fileName + " has expected text");
                } else {
                    System.out.println("Fail " + fileName + " hasn't expected text");
                }
            }
        }
    }

    private String readTextFromFile(String filePath) throws IOException {
        byte[] bytes = Files.readAllBytes(Path.of(filePath));
        return new String(bytes, StandardCharsets.UTF_8);
    }
}

